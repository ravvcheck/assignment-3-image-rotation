//
// Created by RavvCheck1 on 30.10.2023.
//

#include <stdlib.h>

#include "bmp.h"
#include "in_out.h"

#define TYPE 0x4D42
#define RESERVED 0
#define SIZE 40
#define PLANES 1
#define BIT_COUNT 24
#define COMPRESSION 0
#define CRL_USED 0
#define CRL_IMPORTANT 0
#define PELS_PER_METER 2834


enum read_status from_bmp(FILE *in, struct image **img) {
    struct bmp_header *bmp = malloc(sizeof(struct bmp_header));
    if(!bmp){
        return READ_MEMORY_ERROR;
    }
    if (!fread(bmp, sizeof(struct bmp_header), 1, in)) {
        fprintf(stderr, "The header wasn't read!\n");
        return READ_INVALID_HEADER;
    }
    *img = create_image_data(bmp->biWidth, bmp->biHeight);
    if(img == NULL){
        return READ_MEMORY_ERROR;
    }
    for (uint32_t i = 0; i < (*img)->height; i++) {
        if (fread((*img)->data + (*img)->width * i, sizeof(struct pixel), (*img)->width, in) != (*img)->width) {
            fprintf(stderr, "The image hasn't been read!");
            free(bmp);
            return READ_INVALID_BITS;
        }
        if (fseek(in, get_padding((*img)->width), SEEK_CUR) != 0) {
            fprintf(stderr, "The padding hasn't been read!");
            free(bmp);
            return READ_INVALID_BITS;
        }
    }
    free(bmp);
    return READ_OK;
}

enum write_status to_bmp(FILE *out, struct image const *img) {
    struct bmp_header *bmp = malloc(sizeof(struct bmp_header));
    if(!bmp){
        return WRITE_MEMORY_ERROR;
    }
    bmp->bfType = TYPE;
    bmp->bfileSize = sizeof(struct bmp_header) + (uint32_t) img->height * (get_padding(img->width) + img->width * sizeof(struct pixel));
    bmp->bfReserved = RESERVED;
    bmp->bOffBits = sizeof(struct bmp_header);
    bmp->biSize = SIZE;
    bmp->biWidth = img->width;
    bmp->biHeight = img->height;
    bmp->biPlanes = PLANES;
    bmp->biBitCount = BIT_COUNT;
    bmp->biCompression = COMPRESSION;
    bmp->biSizeImage = (uint32_t) img->height * (get_padding(img->width) + img->width * sizeof(struct pixel));
    bmp->biXPelsPerMeter = PELS_PER_METER;
    bmp->biYPelsPerMeter = PELS_PER_METER;
    bmp->biClrUsed = CRL_USED;
    bmp->biClrImportant = CRL_IMPORTANT;
    if (!fwrite(bmp, sizeof(struct bmp_header), 1, out)) {
        fprintf(stderr, "Header hasn't been written!\n");
        free(bmp);
        return WRITE_ERROR;
    }
    for (size_t i = 0; i < img->height; i++) {
        if (!fwrite(img->data + img->width * i, sizeof(struct pixel), img->width, out)) {
            fprintf(stderr, "The image hasn't been written!\n");
            free(bmp);
            return WRITE_ERROR;
        }
        if (fseek(out, get_padding(img->width), SEEK_CUR) != 0 && get_padding(img->width) != 0) {
            fprintf(stderr, "The padding hasn't been written!\n");
            free(bmp);
            return WRITE_ERROR;
        }
    }
    free(bmp);
    return WRITE_OK;
}
