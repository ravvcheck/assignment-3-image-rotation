//
// Created by RavvCheck1 on 30.10.2023.
//

#include <stdint.h>

#include "in_out.h"

FILE *open_file(char *file_name, char *mode) {
    FILE *file = fopen(file_name, mode);
    if (file == NULL) {
        fprintf(stderr, "The file was not found!");
        return NULL;
    }
    return file;
}
