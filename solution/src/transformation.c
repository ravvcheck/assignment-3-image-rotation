//
// Created by RavvCheck1 on 30.10.2023.
//

#include "image.h"
#include "transformation.h"
#include <stdlib.h>

struct image *rotate_quarter_clock(struct image *const source, int angle) {
    uint32_t sourceX;
    uint32_t sourceY;
    uint32_t transformedWidth =
            angle == 90 || angle == 270 || angle == -90 || angle == -270 ? source->height : source->width;
    uint32_t transformedHeight =
            angle == 90 || angle == 270 || angle == -90 || angle == -270 ? source->width : source->height;
    struct image *transformedImg = create_image_data(transformedWidth, transformedHeight);
    if(transformedImg == NULL){
        return NULL;
    }
    for (uint32_t y = 0; y < transformedHeight; y++) {
        for (uint32_t x = 0; x < transformedWidth; x++) {
            switch (angle) {
                case 90:
                case -270:
                    sourceX = source->width - y - 1;
                    sourceY = x;
                    break;
                case 180:
                case -180:
                    sourceX = source->width - x - 1;
                    sourceY = source->height - y - 1;
                    break;
                case -90:
                case 270:
                    sourceX = y;
                    sourceY = source->height - x - 1;
                    break;
                default:
                    sourceX = x;
                    sourceY = y;
                    break;
            }
            uint64_t sourceIndex = (sourceY * source->width + sourceX);
            uint64_t transformedIndex = (y * transformedWidth + x);
            transformedImg->data[transformedIndex] = source->data[sourceIndex];
        }
    }
    return transformedImg;
}
