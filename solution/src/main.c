// #include <stdio.h>
//
#include <bmp.h>

#include <in_out.h>

#include <stdlib.h>
#include <transformation.h>
#define WRITE_MODE "wb"
#define READ_MODE "rb"

int main(int argc, char **argv) {
    if (argc != 4) {
        fprintf(stderr, "Probably the number of arguments: %d! Should be 3!\n", (argc - 1));
        return 1;
    }
    FILE *in = open_file(argv[1], READ_MODE);
    if (in == NULL) {
        return 1;
    }
//

    fprintf(stdout, "File_in was opened!\n");

//
    struct image *img_in = NULL;
    if (from_bmp(in, &img_in) != READ_OK) {
        fprintf(stderr, "Error reading!\n");
        return 1;
    }
//

    fprintf(stdout, "The image has been read!\n");

//
    fclose(in);
    int angle = atoi(argv[3]);
    if (angle % 90 != 0) {
        fprintf(stderr, "The angle must be a multiple 90!\n");
        return 1;
    }
//

    fprintf(stdout, "The angle %d is correct!\n", angle);
//
    struct image *img_out = rotate_quarter_clock(img_in, angle);
    if(img_out == NULL){
        return 1;
    }
//

    fprintf(stdout, "The image was transformered!\n");

//
    FILE *out = open_file(argv[2], WRITE_MODE);
    if (out == NULL) {
        return 1;
    }
//

    fprintf(stdout, "File_out was opened!\n");

//
    if (to_bmp(out, img_out) != WRITE_OK) {
        fprintf(stderr, "Error writing!\n");
        return 1;
    }
//

    fprintf(stdout, "The image has been write!\n");

//
    free_image_data(img_out);
    free(img_out);
    free_image_data(img_in);
    free(img_in);
    fclose(out);
    return 0;
}
