//
// Created by RavvCheck1 on 30.10.2023.
//

#ifndef IMAGE_TRANSFORMER_IO_H
#define IMAGE_TRANSFORMER_IO_H

#include <stdio.h>


FILE *open_file(char *file_name, char *mode);

#endif //IMAGE_TRANSFORMER_IO_H
