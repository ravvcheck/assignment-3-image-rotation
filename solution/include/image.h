//
// Created by RavvCheck1 on 30.10.2023.
//

#ifndef IMAGE_TRANSFORMER_IMAGE_H
#define IMAGE_TRANSFORMER_IMAGE_H

#include <stdint.h>


struct image {
    uint64_t width, height;
    struct pixel *data;
};

struct pixel {
    uint8_t b, g, r;
};

struct image *create_image_data(uint32_t width, uint32_t height);

void free_image_data(struct image *img);

uint32_t get_padding(uint32_t width);

#endif //IMAGE_TRANSFORMER_IMAGE_H
