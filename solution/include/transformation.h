//
// Created by RavvCheck1 on 30.10.2023.
//

#ifndef IMAGE_TRANSFORMER_TRANSFORMATION_H
#define IMAGE_TRANSFORMER_TRANSFORMATION_H

#include <image.h>


struct image *rotate_quarter_clock(struct image *source, int angle);

#endif //IMAGE_TRANSFORMER_TRANSFORMATION_H
